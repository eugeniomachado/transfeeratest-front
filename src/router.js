import Vue from 'vue';
import Router from 'vue-router';

const Home = () => import(`./components/Home.vue`);
const About = () => import(`./components/About.vue`);
const FavouredForm = () => import(`./components/FavouredForm.vue`);

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: `/`,
      name: `home`,
      component: Home,
    },
    {
      path: `/about`,
      name: `about`,
      component: About,
    },
    {
      path: `/favoured/form`,
      name: `favouredform`,
      component: FavouredForm
    },
  ],
  mode: `history`,
});
