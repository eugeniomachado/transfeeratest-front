import { shallowMount, createLocalVue } from '@vue/test-utils'
import App from '@/App'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(VueRouter)
const router = new VueRouter()


describe('App.vue', () => {
  it('has a name', () => {
    expect(App.name).toMatch('App')
  })
  it('is Vue instance', () => {
    const wrapper = shallowMount(App, {
      localVue,
      router
    })
    expect(wrapper.isVueInstance()).toBe(true)
  })
  it('is App', () => {
    const wrapper = shallowMount(App, {
      localVue,
      router
    })
    expect(wrapper.is(App)).toBe(true)
  })
})
