import {
  shallowMount,
  createLocalVue
} from '@vue/test-utils'
import DefaultLayout from '@/layout/DefaultLayout.vue';
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'

const localVue = createLocalVue()
localVue.use(BootstrapVue)
const router = new VueRouter()

describe('DefaultLayout.vue', () => {
  it('has a name', () => {
    expect(DefaultLayout.name).toMatch('DefaultLayout')
  })
  it('has a created hook', () => {
    expect(typeof DefaultLayout.data).toMatch('function')
  })
  test('renders correctly', () => {
    const wrapper = shallowMount(DefaultLayout, {
      localVue,
      router,
    })
    expect(wrapper.element).toMatchSnapshot()
  })
  it('checks the correct default data', () => {
    expect(typeof DefaultLayout.data).toMatch('function')
    const defaultData = DefaultLayout.data()
    expect(typeof defaultData.user).toMatch("object")
  })
  it('is Vue instance', () => {
    const wrapper = shallowMount(DefaultLayout, {
      localVue,
      router
    })
    expect(wrapper.isVueInstance()).toBe(true)
  })
  it('check modal navbar', () => {
    console.log
  })
})
