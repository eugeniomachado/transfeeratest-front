import Vue from 'vue'
import { mount, shallowMount, createLocalVue } from '@vue/test-utils';
import BootstrapVue from 'bootstrap-vue'
import Home from '@/components/Home'
import DefaultLayout from '@/layout/DefaultLayout.vue';
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(VueRouter)
localVue.use(BootstrapVue);
localVue.use(DefaultLayout);
jest.mock('axios', () => {
  return {
    get: () => ({ data: { userId: 1 }})
  }
})

describe('Home.vue', () => {
  it('has a name', () => {
    expect(Home.name).toMatch('home')
  })
  it('has a created hook', () => {
    expect(typeof Home.data).toMatch('function')
  })
})
